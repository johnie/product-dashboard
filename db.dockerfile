FROM mongo

RUN mkdir /usr/local/mongodb-data
ADD db /data/backup

ADD conf/db_entrypoint.sh /db_entrypoint.sh
RUN chmod +x /db_entrypoint.sh
CMD /db_entrypoint.sh
