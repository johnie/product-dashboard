# iZettle - Product Dashboard

> Work assignment for Web Developer position

## Demo

Visit [izettle.crip.io](izettle.crip.io) to see the results and [parse.crip.io:1337/dashboard](parse.crip.io:1337/dashboard) to see the dashboard.

## How to use

First you need [Docker](https://www.docker.com/) installed.

Then in the `.env` you can change the `SERVER_NAME` and `SERVER_URL`(Parse) to your liking. The default is `izettle.dev`. And don't forget to add the same domain(s) in your `/etc/hosts`-file and point to localhost.

**Then run this command:**

```
  $ docker-compose up (-d for detach)
```

Now visit [izettle.dev](izettle.dev) to view the application. And [http://parse.izettle.dev:1337/dashboard](http://parse.izettle.dev:1337/dashboard) to view the Parse Dashboard;

## Frontend

The client solution is built with React and a immutable Redux store with Immutable.js. I've also included `redux-thunk` for async operations. Then I used the [Parse JS SDK](https://github.com/ParsePlatform/Parse-SDK-JS) to communicate with the API and of course the latest ES6 features that is transpiled with Babel using Webpack.

## Backend

**I went with Parse because:**

1. Parse is easy to implement and quick to setup. There are other services like Firebase, which is just as good in most areas, but the database used in Firebase is a simple JSON tree with key/value entries, which is difficult to use in this specific scenario that you are building.

2. Parse offers SDKs for multiple languages, including JS, that makes easier to connect to the backend and create, query, delete objects, etc. You not even need to make REST requests because the Parse SDK does everything for you                        

3. Parse comes with a Dashboard that makes it easy for the developer to quickly model the schemas that he wants for each table (in Parse, called Classes).                        

4. Parse let the developer hosts binary files in the MongoDB database, which is very different from normal relational databases due their GridFS feature, that makes the access to files just as fast as a normal file system
