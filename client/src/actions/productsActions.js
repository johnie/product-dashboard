import * as productsAPI from '../api/products';
import * as actionTypes from '../constants/actionTypes';
import * as statusTypes from '../constants/statusTypes';
import { setRequestStatus, handleRequestError } from '../utilities/requestStatus';

function fetchProductsSuccess(data) {
	return {
		type: actionTypes.FETCH_ALL_PRODUCTS,
		data,
	};
}

export function fetchProducts() {
	let requestPath = `products`;
	return (dispatch) => {
		dispatch(setRequestStatus(requestPath, statusTypes.LOADING));
		productsAPI.fetchAllProducts().then(data => {
			dispatch(fetchProductsSuccess(data));
			dispatch(setRequestStatus(requestPath, statusTypes.SUCCESS));
		}).catch(err => dispatch(handleRequestError(requestPath, err)));
	}
}


function fetchProductSuccess(data) {
	return {
		type: actionTypes.FETCH_PRODUCT,
		data,
	};
}

export function fetchProduct(id) {
	let requestPath = `product`;
	return (dispatch) => {
		dispatch(setRequestStatus(requestPath, statusTypes.LOADING));
		productsAPI.fetchProduct(id).then(data => {
			dispatch(fetchProductSuccess(data));
			dispatch(setRequestStatus(requestPath, statusTypes.SUCCESS));
		}).catch(err => dispatch(handleRequestError(requestPath, err)));
	}
}


function createProductSuccess(data) {
	return {
		type: actionTypes.CREATE_PRODUCT,
		data,
	};
}

export function createProduct(image, name, price, total) {
	let requestPath = `products_create`;
	return (dispatch) => {
		dispatch(setRequestStatus(requestPath, statusTypes.LOADING));
		productsAPI.createProduct(image, name, price, total).then(data => {
			dispatch(createProductSuccess(data));
			dispatch(setRequestStatus(requestPath, statusTypes.SUCCESS));
		}).catch(err => dispatch(handleRequestError(requestPath, err)));
	}
}


function removeProductSuccess(id, data) {
	return {
		type: actionTypes.REMOVE_PRODUCT,
		id,
		data,
	};
}

export function removeProduct(id) {
	let requestPath = `products_remove`;
	return (dispatch) => {
		dispatch(setRequestStatus(requestPath, statusTypes.LOADING));
		productsAPI.removeProduct(id).then(data => {
			dispatch(removeProductSuccess(id, data));
			dispatch(setRequestStatus(requestPath, statusTypes.SUCCESS));
		}).catch(err => dispatch(handleRequestError(requestPath, err)));
	}
}


function updateProductSuccess(id, data) {
	return {
		type: actionTypes.UPDATE_PRODUCT,
		id,
		data,
	};
}

export function updateProduct(id, update) {
	let requestPath = `products_update`;
	return (dispatch) => {
		dispatch(setRequestStatus(requestPath, statusTypes.LOADING));
		productsAPI.updateProduct(id, update).then(data => {
			dispatch(updateProductSuccess(id, data));
			dispatch(setRequestStatus(requestPath, statusTypes.SUCCESS));
		}).catch(err => dispatch(handleRequestError(requestPath, err)));
	}
}
