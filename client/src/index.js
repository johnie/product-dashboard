import React from 'react';
import ReactDOM from 'react-dom';
import Router from './containers/RouterContainer';
import './styles/main.scss';

ReactDOM.render(
    <Router />,
    document.getElementById('root')
);
