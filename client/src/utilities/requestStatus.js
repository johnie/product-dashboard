import * as actionTypes from '../constants/actionTypes';
import * as statusTypes from '../constants/statusTypes';

export const setRequestStatus = (id, status) => {

	if(id.indexOf('?') !== -1 || id.indexOf('&') !== -1 ){
		console.warn('RequestStatus key should not contain query string parameters.', id)
	}

	if(id.split('').pop() === '/'){
		console.warn('RequestStatus key should not contain trailing slash.', id)
	}

	return {
    type: actionTypes.REQUEST_SET_STATUS,
    id,
    status
  };
};

export const handleRequestError = (id, err) => {
  switch (err.code) {
    case statusTypes.PERMISSION_DENIED:
      return setRequestStatus(id, statusTypes.PERMISSION_DENIED);
    default:
      return setRequestStatus(id, statusTypes.FAIL);
  }
};
