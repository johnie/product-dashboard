const filterUndefinedFromObject = (obj) => {
	Object.keys(obj).forEach(key => {
		if (obj[key] && typeof obj[key] === 'object') {
			filterUndefinedFromObject(obj[key]);
		} else if (obj[key].toString() === 'undefined') {
			delete obj[key];
		}
	});
	return obj;
};

export default filterUndefinedFromObject;
