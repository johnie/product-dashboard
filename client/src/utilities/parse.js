import Parse from 'parse';
import {config} from '../config';

const parse = Parse;

parse.initialize(config.parse.apiId);
parse.serverURL = config.parse.serverURL;

export default parse;

