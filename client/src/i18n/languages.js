export const en = {
	productListHeader: {
		image: 'Image',
		name: 'Product name',
		price: 'Price',
		total: 'Total',
		edit: 'Edit'
	},
	productList: {
		noProductsTitle: 'No products found.',
		title: 'Product library',
		imagePlaceholder: 'Product image',
		addProduct: 'Add product',
		currency: '€',
	},
	handleProduct: {
		modalLabel: 'Edit product',
		name: 'Product name',
		price: 'Price',
		total: 'Total',
		saveButton: 'Save Product',
		removeButton: 'Remove Product',
	}
};
