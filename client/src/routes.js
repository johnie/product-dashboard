import connect from './store/connect';

/**
 * Actions
 */
import * as productsActions from './actions/productsActions';

/**
 * Components
 */
import App from './components/App/App';
import HandleProduct from './components/HandleProduct/HandleProduct';

/**
 * Pages
 */
import StartPage from './pages/StartPage/StartPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

const routes = {
  component: connect(App),
  childRoutes: [
    {
      path: '/',
      component: connect(StartPage, {...productsActions}),
			childRoutes: [
				{
					path: '/publish',
					component: connect(HandleProduct, {...productsActions}),
				},
				{
					path: 'edit/:productId',
					component: connect(HandleProduct, {...productsActions}),
				},
			]
    },
    {
        path: '*',
        component: connect(NotFoundPage)
    }
  ],
};

export default routes;
