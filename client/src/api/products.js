import parse from '../utilities/parse';
import {parallel} from 'async';

/**
 * Products class
 */
const Products = parse.Object.extend('products');


/**
 * Fetch all products
 * @return {promise}
 */
export const fetchAllProducts = () => new Promise((resolve, reject) => {
	const query = new parse.Query(Products);
	query.ascending('createdAt');

	return query.find().then((data) => {
		resolve(data);
	}, error => reject(error))
});


/**
 * Fetch on product
 * @param {string} id Product ID
 * @return {promise}
 */
export const fetchProduct = (id) => new Promise((resolve, reject) => {
	const query = new parse.Query(Products);

	query.equalTo('objectId', id);

	return query.find().then((data) => {
		resolve(...data);
	}, error => reject(error))
});


/**
 * Create one product
 * @param {File} image Product Image
 * @param {string} name Product Name
 * @param {string} price Product Price
 * @param {number} total Product Total
 * @return {promise}
 */
export const createProduct = (image, name, price, total) => new Promise((resolve, reject) => {
	const product = new Products();

	return parallel([
		callback => {
			product.set('product_name', name);
			product.set('product_price', price);
			product.set('product_total', total);

			callback(null, product);
		},
		callback => {
			if (!image) {
				callback(null, false);
				return;
			}

			const parseImage = new parse.File(image.name, image);

			parseImage.save().then(function() {
				callback(null, parseImage);
			}, function(error) {
				reject(error);
			});
		}
		], (err, results) => {
			const productMeta = results[0];
			const productImage = results[1];

			if (productImage) {
				productMeta.set('product_image', productImage);
			}

			productMeta.save({
				success: function (item) {
					resolve(item);
				},
				error: function (err) {
					reject(err);
				}
			});

		}
	);
});


/**
 * Remove one product
 * @param {string} id The product ID
 * @return {promise}
 */
export const removeProduct = (id) => new Promise((resolve, reject) => {
	const query = new parse.Query(Products);

	query.equalTo('objectId', id);

	return query.find().then((data) => {
		resolve(...data);
		data[0].destroy();
	}, error => reject(error));
});


/**
 * @param {string} id The product item id
 * @param {object} update The data to update the product with
 * @return {promise}
 */
export const updateProduct = (id, update) => new Promise((resolve, reject) => {
	const query = new parse.Query(Products);

	query.equalTo('objectId', id);

	return query.find().then((data) => {
		data[0].save(update, {
			success(obj) {
				resolve(obj);
			},
			error(obj, error) {
				reject(error, obj);
			}
		});
	}, error => reject(error));
});
