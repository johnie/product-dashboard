import React from 'react';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from '../routes';
import { syncHistoryWithStore } from 'react-router-redux';

import configureStore from '../store/configureStore';
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

const RouterContainer = () => (
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>
);

export default RouterContainer;
