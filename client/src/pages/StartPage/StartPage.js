import React, { Component } from 'react';
import style from './startPage.scss';

// Components
import ProductList from '../../components/ProductList/ProductList';

class StartPage extends Component {

  componentDidMount() {
  	const { fetchProducts } = this.props;
		fetchProducts();
  }

  render() {
  	const {products, requestsStatus, children} = this.props;
  	return (
  		<div className={style.root}>
				<ProductList products={products} status={requestsStatus.get('products')}></ProductList>
				{(children) && children}
			</div>
		);
  }
}


export default StartPage;
