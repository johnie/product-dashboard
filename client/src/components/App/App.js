import
	React, {
	Component,
	PropTypes
} from 'react';
import style from './app.scss';
import {setI18n} from 'get-i18n';
import {en} from '../../i18n/languages';

// Components
import Header from '../Header/Header';

class App extends Component {
	componentDidMount() {
		setI18n(en);
	}

	render() {
		const {children} = this.props;

		return (
			<div className={style.root} role="document">
				<Header />
				<main className={style.main} role="main">
					{children}
				</main>
			</div>
		)
	}
}

App.propTypes = {
	children: PropTypes.object,
};

export default App;
