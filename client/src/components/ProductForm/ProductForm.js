import
	React, {
	Component
} from 'react';
import style from './productForm.scss';
import filterUndefinedFromObject from '../../utilities/filterUndefinedFromObject';
import {getI18n} from 'get-i18n';

// Components

class ProductForm extends Component {
	constructor(props) {
		super(props);

		this.onFormChange = this.onFormChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleDelete = this.handleDelete.bind(this);

		this.state = {
			product_name: props.initialProduct.get('product_name'),
			product_price: props.initialProduct.get('product_price'),
			product_total: props.initialProduct.get('product_total'),
		};
	}

	onFormChange(field, value) {
		this.setState({ [field]: value });
	}

	handleSubmit(e) {
		e.preventDefault();
		const {handleSubmit} = this.props;
		handleSubmit(filterUndefinedFromObject(this.state));
	}

	handleDelete(e) {
		e.preventDefault();
		const {handleRemove} = this.props;
		if (confirm('Do you want to delete product?')) {
			handleRemove();
		}
	}

	handleChange(newValue){
		this.setState(({amount}) => ({newValue}));
	}

	componentDidMount() {
	}

	render() {
		const {params} = this.props;

		const {
			product_name,
			product_price,
			product_total
		} = this.state;

		return (
			<div className={style.productForm}>
				<form onSubmit={this.handleSubmit}>
					<fieldset>
						<label htmlFor="name">{getI18n('handleProduct.name')}</label>
						<input
							type="text"
							id="product_name"
							name="product_name"
							placeholder="Chocolate cupcake"
							defaultValue={product_name}
							onChange={(e) => this.onFormChange('product_name', e.target.value)}
							required
						/>
					</fieldset>

					<fieldset>
						<label htmlFor="price">{getI18n('handleProduct.price')}</label>
						<input
							type="number"
							id="product_price"
							name="product_price"
							min="1"
							step="any"
							placeholder="7,90"
							defaultValue={product_price}
							onChange={(e) => this.onFormChange('product_price', e.target.value)}
							required
						/>
					</fieldset>

					<fieldset>
						<label htmlFor="total">{getI18n('handleProduct.total')}</label>
						<input
							type="number"
							id="total"
							name="product_total"
							min="1"
							step="1"
							placeholder="90"
							defaultValue={product_total}
							onChange={(e) => this.onFormChange('product_total', parseInt(e.target.value, 0))}
							required
						/>
					</fieldset>

					<div className={style.productFormButtons}>
						{params.productId ?
							<button
								className={style.productFormRemove}
								type="button"
								onClick={this.handleDelete}
							>
								{getI18n('handleProduct.removeButton')}
							</button>
							: null}

						<button
							className={style.productFormSave}
							type="submit"
						>
							{getI18n('handleProduct.saveButton')}
						</button>

					</div>
				</form>
			</div>
		)
	}
}

ProductForm.propTypes = {
	params: React.PropTypes.object.isRequired,
	initialProduct: React.PropTypes.shape({
		product_name: React.PropTypes.string,
		product_price: React.PropTypes.string,
		product_total: React.PropTypes.number,
	}),
};

export default ProductForm;
