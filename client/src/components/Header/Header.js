import React from 'react';
import { IndexLink } from 'react-router';
import style from './header.scss';
import logo from '../..//assets/icons/logo.svg';

const Header = () => {
	return (
		<header className={style.header} role="banner">
			<div className={style.headerInner}>
				<IndexLink to="/" className={style.headerLogo}>
					<img src={logo} alt="Logo" />
				</IndexLink>
			</div>
		</header>
	)
}

export default Header;
