import React, { Component } from 'react';
import {Link} from 'react-router';
import style from './productList.scss';
import * as statusTypes from '../../constants/statusTypes';
import {getI18n} from 'get-i18n';
import addIcon from '../../assets/icons/add.svg';

// Components
import Loading from '../Loading/Loading';
import NoProducts from '../NoProducts/NoProducts';
import ProductListItem from '../ProductListItem/ProductListItem';
import ProductListHeader from '../ProductListHeader/ProductListHeader';

class ProductList extends Component {

	componentDidMount() {
	}

	render() {
		const {
			products,
			status
		} = this.props;
		switch (status) {
			case statusTypes.LOADING:
				return (<Loading/>);
			case statusTypes.SUCCESS:
				return (
					<div className={style.productListWrap}>
						<div className={style.productListHeader}>
							<h2>{getI18n('productList.title')}</h2>
							<Link to="/publish" className={style.addProduct}>
								<img src={addIcon} className={style.addProductIcon} alt={getI18n('productList.addProduct')} />
								{getI18n('productList.addProduct')}
							</Link>
						</div>
						<div className={style.productList}>
							<ProductListHeader />
							{products.map((product, index) => {
								return (
									<ProductListItem item={product} key={`${index}-product-${product.id}`} />
								)
							})}
							{(products.length === 0) && <NoProducts />}
						</div>
					</div>
				);
			default:
				return (<Loading/>);
		}
	}
}

export default ProductList;
