import React from 'react';
import {Link} from 'react-router';
import style from './productListItem.scss';
import editIcon from '../../assets/icons/edit.svg';
import {getI18n} from 'get-i18n';
import cx from 'classnames';

// Components
import ProductImage from '../ProductImage/ProductImage';

const ProductListItem = ({item}) => {
	const itemWrap = cx(style.inner, style.row);
	const image = (item.get('product_image')) ? item.get('product_image').url() : false;
	return (
		<div className={style.productItem}>
			<div className={itemWrap}>
				<div className={style['col-sm-2']}>
					<ProductImage src={image} className={style.productImage} />
				</div>

				<div className={style['col-sm-4']}>
					{item.get('product_name')}
				</div>

				<div className={style['col-sm-2']}>
					{getI18n('productList.currency')}{item.get('product_price')}
				</div>

				<div className={style['col-sm-2']}>
					{item.get('product_total')}
				</div>

				<div className={style['col-sm-2']}>
					<Link to={`/edit/${item.id}`} className={style.productItemEdit}>
						<img src={editIcon} alt={`Edit ${item.get('product_name')}`} />
					</Link>
				</div>
			</div>
		</div>
	);
}

export default ProductListItem;
