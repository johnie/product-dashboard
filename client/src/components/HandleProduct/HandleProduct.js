import
	React, {
	Component
} from 'react';
import {Link, browserHistory} from 'react-router';
import {fromJS} from 'immutable';
import style from './handleProduct.scss';
import * as statusTypes from '../../constants/statusTypes';
import {getI18n} from 'get-i18n';

// Components
import Modal from 'react-modal';
import DropZone from 'react-dropzone';
import Loading from '../Loading/Loading';
import ProductImage from '../ProductImage/ProductImage';
import ProductForm from '../ProductForm/ProductForm';

class HandleProduct extends Component {
	constructor(props) {
		super(props);
		this.handleSave = this.handleSave.bind(this);
		this.handleRemove = this.handleRemove.bind(this);
		this.handleDrop = this.handleDrop.bind(this);

		this.state = {
			product_image: false
		};
	}

	componentDidMount() {
		const {
			fetchProduct,
			params,
		} = this.props;

		if (params.productId) fetchProduct(params.productId);
	}

	componentWillReceiveProps(nextProps) {
		const { fetchProduct, params } = this.props;
		if (params !== nextProps.params) {
			fetchProduct(nextProps.params.productId);
		}
	}

	handleSave(data) {
		// Props
		const {
			createProduct,
			updateProduct,
			params
		} = this.props;

		// State
		const {product_image} = this.state;

		// Data
		const {
			product_name,
			product_price,
			product_total
		} = data;

		if (typeof params.productId !== 'undefined') {
			updateProduct(params.productId, data);
		} else {
			createProduct(product_image, product_name, product_price, product_total);
		}

		browserHistory.push({
			pathname: '/'
		});
	}

	handleDrop(file) {
		if (!file) return;
		this.setState({product_image: file[0]});
	}

	handleRemove() {
		const {removeProduct, params} = this.props;
		removeProduct(params.productId);

		browserHistory.push({
			pathname: '/'
		});
	}

	render() {
		const {product, params, requestsStatus, location} = this.props;
		const isPublishPage = (location.pathname === '/publish');
		const theProduct = !isPublishPage ? product : fromJS({});
		const status = requestsStatus.get('product');
		const image = (product.get('product_image') && !isPublishPage) ? product.get('product_image').url() : false;
		const modal = (
			<Modal
				isOpen={true}
				overlayClassName={style.overlay}
				className={style.content}
				contentLabel={getI18n('handleProduct.modalLabel')}
			>
				<Link to="/" className={style.closeModal}>✕</Link>
				<DropZone
					className={style.productImageDropZone}
					multiple={false}
					accept="image/*"
					name="image"
					onDrop={(e) => this.handleDrop(e)}
				>
					<ProductImage src={image} />
				</DropZone>
				<ProductForm
					params={params}
					initialProduct={theProduct}
					handleSubmit={(data) => this.handleSave(data)}
					handleRemove={this.handleRemove}
				/>
			</Modal>
		);

		switch (status) {
			case statusTypes.LOADING:
				return (<Loading/>);
			case statusTypes.SUCCESS:
				return modal;
			default:
				return modal;
		}
	}
}

HandleProduct.propTypes = {
	product: React.PropTypes.object.isRequired,
	params: React.PropTypes.object.isRequired,
	requestsStatus: React.PropTypes.object.isRequired,
	fetchProduct: React.PropTypes.func.isRequired,
};

export default HandleProduct;
