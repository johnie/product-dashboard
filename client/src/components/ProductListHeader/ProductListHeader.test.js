import React from 'react';
import ReactDOM from 'react-dom';
import ProductListHeader from './ProductListHeader';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<ProductListHeader />, div);
});
