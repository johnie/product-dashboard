import React from 'react';
import style from './productListHeader.scss';
import {getI18n} from 'get-i18n';
import cx from 'classnames';

const ProducListHeader = () => {
	const productHeader = cx(style.productListHeader, style.row);
	const productHeaderItem = (width) => cx(style.productListHeaderItem, style[`col-sm-${width}`]);
	return (
		<div className={productHeader}>
			<div className={productHeaderItem('2')}>{getI18n('productListHeader.image')}</div>
			<div className={productHeaderItem('4')}>{getI18n('productListHeader.name')}</div>
			<div className={productHeaderItem('2')}>{getI18n('productListHeader.price')}</div>
			<div className={productHeaderItem('2')}>{getI18n('productListHeader.total')}</div>
			<div className={productHeaderItem('2')}>{getI18n('productListHeader.edit')}</div>
		</div>
	);
};

export default ProducListHeader;
