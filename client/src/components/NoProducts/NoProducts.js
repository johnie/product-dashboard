import React from 'react';
import style from './noProducts.scss';
import {getI18n} from 'get-i18n';

const NoProducts = () => (
	<div className={style.noProducts}>
		<h2>{getI18n('productList.noProductsTitle')}</h2>
	</div>
);

export default NoProducts;
