import React from 'react';
import ReactDOM from 'react-dom';
import noProducts from './noProducts';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<noProducts />, div);
});
