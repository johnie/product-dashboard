import React from 'react';
import {getI18n} from 'get-i18n';
import imagePlaceholder from '../../assets/icons/product_placeholder.svg';

const ProductImage = (props) => {
	return (
		<div className={props.className}>
			{props.src && (
				<img src={props.src} alt={props.alt}/>
			)}
			{!props.src && (
				<img src={imagePlaceholder} alt={getI18n('productList.imagePlaceholder')}/>
			)}
		</div>
	);
};

ProductImage.propTypes = {
	src: React.PropTypes.any,
	className: React.PropTypes.string,
	alt: React.PropTypes.string,
	size: React.PropTypes.number,
};

export default ProductImage;
