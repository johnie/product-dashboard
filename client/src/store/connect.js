import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(actions, dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default (component, actions = {}) => {
  return connect(mapStateToProps, mapDispatchToProps.bind(this, actions))(component);
};
