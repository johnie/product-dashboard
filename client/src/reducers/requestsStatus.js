import {fromJS} from 'immutable';
import * as actionTypes from '../constants/actionTypes';

export default function requestsStatus(state = fromJS({}), action) {
  switch (action.type) {
    case actionTypes.REQUEST_SET_STATUS:
      return state.set(action.id, action.status);
    default:
      return state;
  }
}
