import {fromJS} from 'immutable';
import * as actionTypes from '../constants/actionTypes';

export default function products(state = fromJS({}), action) {
  switch (action.type) {
		case actionTypes.FETCH_ALL_PRODUCTS:
      return action.data;
		case actionTypes.CREATE_PRODUCT:
			return state.concat(action.data);
		case actionTypes.REMOVE_PRODUCT:
			return state.filter(product => product.id !== action.id);
		case actionTypes.UPDATE_PRODUCT:
			return state.map(product => {
				if(product.id !== action.id) {
					return product;
				}

				return action.data;
			});
		default:
      return state;
  }
}

export function product(state = fromJS({}), action) {
	switch (action.type) {
		case actionTypes.FETCH_PRODUCT:
			return action.data;
		default:
			return state;
	}
}
