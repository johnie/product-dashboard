import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';
import requestsStatus from './requestsStatus';
import products, {product} from './products';

const rootReducer = combineReducers({
  requestsStatus,
	products,
	product,
	routing: routerReducer,
});

export default rootReducer;
