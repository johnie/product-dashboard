
export const REQUEST_SET_STATUS = 'REQUEST_SET_STATUS';

export const FETCH_ALL_PRODUCTS = 'FETCH_ALL_PRODUCTS';

export const FETCH_PRODUCT = 'FETCH_PRODUCT';

export const CREATE_PRODUCT = 'CREATE_PRODUCT';

export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';

export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

// On location change
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';
