export const LOADING = 'LOADING';
export const UPDATING = 'UPDATING';
export const FAIL = 'FAIL';
export const SUCCESS = 'SUCCESS';
export const PERMISSION_DENIED = 'PERMISSION_DENIED';
