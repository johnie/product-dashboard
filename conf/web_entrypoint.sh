#!/bin/bash

# Copy server name to vhost
sed -i 's/SERVER_NAME/'"${SERVER_NAME}"'/g' /etc/nginx/conf.d/web.conf
sed -i 's/SERVER_NAME/'"${SERVER_NAME}"'/g' /etc/nginx/conf.d/parse.conf
sed -i 's/SERVER_NAME/'"${SERVER_NAME}"'/g' /var/www/web/src/config.js

# Build project
cd /var/www/web
npm install
npm run build

# Run nginx
nginx -g "daemon off;"
