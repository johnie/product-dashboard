#!/bin/bash

# Import database
(sleep 5;
mongorestore --host mongo --port 27017 --db izettle /data/backup
)&

mongod --dbpath /usr/local/mongodb-data
