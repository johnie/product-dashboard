FROM node
MAINTAINER Johnie Hjelm <johnie@hjelm.im>

ADD express /usr/local/parse
WORKDIR /usr/local/parse
RUN npm install

CMD npm start
